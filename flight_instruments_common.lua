script_name("flight instruments common module")
script_description("provides common functionality between flight instruments scripts")
script_author("miko-chan")
script_moonloader(26)

local fltCommon = {}

fltCommon.readings = {
    airSpeed = 0.0,
    radarAltitude = 0.0,
    radioAltitude = 0.0,
    bankAngle = 0.0,
    pitch = 0.0,
    zoneId = nil,
}
fltCommon.controls = {
    autopilot = false,
    landingGearUp = false
}

fltCommon.textColors = {
    CRITICAL = { 240, 0, 0 },
    WARN = { 255, 255, 128 },
    NORMAL = { 255, 255, 255 },
    OUTLINE = { 0, 0, 0 },
}
fltCommon.textPosAlign = {
    LEFT = 0,
    CENTER = 1,
    RIGHT = 2,
}
fltCommon.textStyle = {
    GOTHIC = 0,
    NORMAL = 1,
    MENU = 2,
    PRICEDOWN = 3
}

function fltCommon.isPlayerInAircraft()
    return isCharInFlyingVehicle(PLAYER_PED) and getActiveInterior() == 0 and not isPlayerDead(PLAYER_HANDLE)
end

function fltCommon.isPlayerInVehicle()
    return isCharInAnyCar(PLAYER_PED) and getActiveInterior() == 0 and not isPlayerDead(PLAYER_HANDLE)
end

function fltCommon.round(num, numDecimalPlaces)
    local mult = 10 ^ (numDecimalPlaces or 0)
    return math.floor(num * mult + 0.5) / mult
end

function fltCommon.clamp(num, minVal, maxVal)
    return math.max(minVal, math.min(num, maxVal))
end

function fltCommon.mpsToKnots(mpsUnit)
    return mpsUnit * 1.944
end

function fltCommon.knotsToMps(knotsUnit)
    return knotsUnit / 1.944
end

function fltCommon.metersToFeet(metersUnit)
    return metersUnit * 3.281
end

function fltCommon.feetToMeters(feetUnit)
    return feetUnit / 3.281
end

-- Start: Timer class
fltCommon.Timer = {}
fltCommon.Timer.__index = fltCommon.Timer
function fltCommon.Timer:new()
    local timer = {}
    setmetatable(timer, fltCommon.Timer)
    timer:reset()
    return timer
end

function fltCommon.Timer:waitFor(timeMs)
    return self:getProgress(timeMs) == 1
end

function fltCommon.Timer:getProgress(timeMs)
    local currentTick = localClock() * 1000
    self.__current = currentTick
    if self.__start == 0 then
        self.__start = currentTick
    end
    local progress = (self.__current - self.__start) / timeMs
    return fltCommon.clamp(progress, 0, 1)
end

function fltCommon.Timer:reset()
    self.__current = 0
    self.__start = 0
end
-- End: Timer class

-- Start: DisplayText class
fltCommon.DisplayText = {}
fltCommon.DisplayText.__index = fltCommon.DisplayText
function fltCommon.DisplayText:new(fxtEntry, properties)
    local dispText = {}
    setmetatable(dispText, fltCommon.DisplayText)
    properties = properties or {}
    dispText.fxtEntry = fxtEntry
    dispText.opacity = properties.opacity or 255
    dispText.style = properties.style or fltCommon.textStyle.NORMAL
    dispText.scale = properties.scale or { 1.0, 1.0 }
    dispText.outlineSize = properties.outlineSize or 2
    dispText.outlineColor = properties.outlineColor or fltCommon.textColors.OUTLINE
    dispText.color = properties.color or fltCommon.textColors.NORMAL
    dispText.wrapLength = properties.wrapLength or 0
    dispText.posAlign = properties.posAlign or fltCommon.textPosAlign.LEFT
    dispText.defaultDrawPos = properties.defaultDrawPos or { 0.0, 0.0 }
    dispText.visible = properties.visible or true
    dispText.__fadeTimer = fltCommon.Timer:new()
    return dispText
end

function fltCommon.DisplayText:drawText(x, y)
    if not self.visible then 
        return 
    end
    x, y = self:__prepareText(x, y)
    displayText(x, y, self.fxtEntry)
end

function fltCommon.DisplayText:drawInteger(num, x, y)
    if not self.visible then 
        return 
    end
    x, y = self:__prepareText(x, y)
    displayTextWithNumber(x, y, "NUMBER", num)
end

function fltCommon.DisplayText:drawFloat(num, precision, x, y)
    if not self.visible then 
        return 
    end
    x, y = self:__prepareText(x, y)
    local whole, frac = math.modf(num)
    precision = precision or 2
    frac = math.floor(fltCommon.round(frac, precision) * (10 ^ precision))
    displayTextWith2Numbers(x, y, "FLTDSPF", whole, math.abs(frac))
end

function fltCommon.DisplayText:fadeIn(timeMs)
    return self:__fade(timeMs, false)
end

function fltCommon.DisplayText:fadeOut(timeMs)
    return self:__fade(timeMs, true)
end

function fltCommon.DisplayText:resetFade()
    self.__fadeTimer:reset()
    self.opacity = 255
end

function fltCommon.DisplayText:__prepareText(x, y)
    setTextFont(self.style)
    setTextScale(table.unpack(self.scale))
    setTextEdge(self.outlineSize, getRGBA(self.outlineColor, self.opacity))
    setTextColour(getRGBA(self.color, self.opacity))
    setTextWrapx(self.wrapLength)
    if self.posAlign == 0 then
        setTextJustify(false)
    elseif self.posAlign == 1 then
        setTextCentre(true)
    else
        setTextRightJustify(true)
    end
    setTextDrawBeforeFade(true)
    return x or self.defaultDrawPos[1], y or self.defaultDrawPos[2]
end

function fltCommon.DisplayText:__fade(timeMs, fadeOut)
    local progress = self.__fadeTimer:getProgress(timeMs)
    local percentAlpha = progress
    if fadeOut then
        percentAlpha = 1 - percentAlpha
    end
    self.opacity = percentAlpha * 255.0
    return progress == 1
end
-- End: DisplayText class

function getRGBA(rgb, a)
    local r, g, b = table.unpack(rgb)
    return r, g, b, a
end

return fltCommon
