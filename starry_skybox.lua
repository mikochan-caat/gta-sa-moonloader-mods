script_name("Starry Skybox")
script_description("Starry skybox without the save graffiti bug")
script_author("Miko-chan")
script_moonloader(26)

local memory = require 'memory'
local moonAdds = require 'MoonAdditions'

local graffitiModelId = 4227
local starHolderObj = nil
local starObj = nil

function main()
    print("Executing main loop...")
    while true do
        wait(10)
        if isPlayerPlaying(PLAYER_HANDLE) then
            requestModel(graffitiModelId)
            loadAllModelsNow()
            
            starHolderObj = createObject(graffitiModelId, 1.0, 1.0, -32.0)
            makeObjectUnsaveable(starHolderObj)
            
            starObj = createObject(graffitiModelId, 1.0, 1.0, -32.0)
            makeObjectUnsaveable(starObj)
            
            setObjectCollision(starHolderObj, false)
            setObjectCollision(starObj, false)
            
            print("Created starry skybox objects.")
            while starHolderObj ~= nil and starObj ~= nil do
                wait(100)
                updateSkyboxObjectsState()
            end
        end
    end
end

function updateSkyboxObjectsState()
    local hh, mm = getTimeOfDay()
    local weatherTicks = memory.getfloat(0xC8130C, false)
    local canDraw = isStarsAllowedForCurrentWeather() and not isInInterior() and not isInAnyMenu()
    
    local fadeAlpha = 0.0
    local isStarsVisible = true
    if canDraw then
        if hh == 22 then
            fadeAlpha = math.floor(weatherTicks * 255.0)                -- Fade in at 22:00
            fadeAlpha = math.min(fadeAlpha + 45.0, 255.0)
        elseif hh == 4 then
            fadeAlpha = math.floor((weatherTicks  * -255.0) + 255.0)    -- Fade out at 05:00
        elseif hh > 22 or hh <= 3 then
            fadeAlpha = 255.0
        end
        
        local x, y, z = getOffsetFromCharInWorldCoords(PLAYER_PED, 0.0, 0.0, 0.0)
        setObjectCoordinates(starHolderObj, x, y, z)
        attachObjectToObject(starObj, starHolderObj, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0) 
    else
        isStarsVisible = false
    end
    
    setObjectVisible(starHolderObj, isStarsVisible)
    setObjectVisible(starObj, isStarsVisible)
    moonAdds.set_object_model_alpha(starHolderObj, fadeAlpha)
    moonAdds.set_object_model_alpha(starObj, fadeAlpha)
end

function isStarsAllowedForCurrentWeather()
    local currentWeatherId = readMemory(0xC81320, 2, false)
    return -- Ranges and conditions below:
        (currentWeatherId >= 0 and currentWeatherId <= 3) or    -- LS_Sunny
        (currentWeatherId == 5 or currentWeatherId == 6) or     -- SF_Sunny
        (currentWeatherId == 10 or currentWeatherId == 11) or   -- LV_Sunny
        (currentWeatherId == 13 or currentWeatherId == 14) or   -- CS_Sunny
        (currentWeatherId == 17 or currentWeatherId == 18)      -- DS_Sunny
end

function isInInterior()
    return getActiveInterior() ~= 0
end

function isInAnyMenu()
    local menuOpenedBit = memory.read(0xBA67A4, 1, false)
    return menuOpenedBit == 1
end

function makeObjectUnsaveable(obj)
    local objPtr = getObjectPointer(obj)
    objPtr = objPtr + 0x13C
    memory.write(objPtr, 6, 1)
end
